document.addEventListener('DOMContentLoaded', () => {
  const searchInput = document.getElementById('searchInput')
  const autocompleteList = document.getElementById('autocompleteList')
  const searchHistory = document.getElementById('searchHistory')
  const clearHistoryButton = document.getElementById('clearHistoryButton')
  const API_URL = 'https://jsonplaceholder.typicode.com/posts' // Sample public API for demo purposes

  let debounceTimeout

  searchInput.addEventListener('input', (e) => {
    const query = e.target.value.trim()
    if (query) {
      clearTimeout(debounceTimeout)
      debounceTimeout = setTimeout(() => fetchResults(query), 300)
    } else {
      clearAutocomplete()
    }
  })

  autocompleteList.addEventListener('click', (e) => {
    if (e.target && e.target.nodeName === 'LI') {
      const selectedItem = e.target
      const title = selectedItem.textContent
      saveToHistory(title)
      clearAutocomplete()
    }
  })

  clearHistoryButton.addEventListener('click', () => {
    searchHistory.innerHTML = ''
    localStorage.removeItem('searchHistory')
  })

  function fetchResults(query) {
    fetch(`${API_URL}?q=${query}`)
      .then((response) => response.json())
      .then((data) => showAutocomplete(data, query))
      .catch((error) => console.error('Error fetching data:', error))
  }

  function showAutocomplete(results, query) {
    clearAutocomplete()
    const regex = new RegExp(`(${query})`, 'gi')
    results.slice(0, 5).forEach((item) => {
      const li = document.createElement('li')
      const highlightedTitle = item.title.replace(regex, '<span class="highlight">$1</span>')
      li.innerHTML = highlightedTitle
      li.className = 'autocompleteItem'
      autocompleteList.appendChild(li)
    })
  }

  function clearAutocomplete() {
    autocompleteList.innerHTML = ''
  }

  function saveToHistory(title) {
    const timestamp = new Date().toLocaleString()
    const li = document.createElement('li')
    li.className = 'historyItem'
    li.innerHTML = `${title} <span class="timestamp">${timestamp}</span>`
    searchHistory.appendChild(li)
    saveHistoryToLocalStorage()
  }

  function saveHistoryToLocalStorage() {
    const historyItems = []
    searchHistory.querySelectorAll('.historyItem').forEach((item) => {
      historyItems.push(item.innerHTML)
    })
    localStorage.setItem('searchHistory', JSON.stringify(historyItems))
  }

  function loadHistoryFromLocalStorage() {
    const historyItems = JSON.parse(localStorage.getItem('searchHistory'))
    if (historyItems) {
      historyItems.forEach((itemHTML) => {
        const li = document.createElement('li')
        li.className = 'historyItem'
        li.innerHTML = itemHTML
        searchHistory.appendChild(li)
      })
    }
  }

  // Initial load
  loadHistoryFromLocalStorage()
})
